package com.homeplus.repositories;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.TimeSectionsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface TimeSectionsRepository extends JpaRepository<TimeSectionsEntity, Long> {

    @Query("select u from TimeSectionsEntity as u where u.id = :id")
    Optional<TimeSectionsEntity> findById(@Param("id") Long id);
}
