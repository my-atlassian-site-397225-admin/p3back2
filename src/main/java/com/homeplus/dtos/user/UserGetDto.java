package com.homeplus.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserGetDto {
    private Long id;

    private String email;

    private String name;

    private String gender;

    private String language;

    private String state;

    private String street;

    private String postcode;

    private Date date_of_birth;

    private Integer mobile;

    private Boolean is_tasker;

    private Boolean is_tasker_data;

    private String status;

    private OffsetDateTime createdTime;

    private OffsetDateTime updatedTime;
}
