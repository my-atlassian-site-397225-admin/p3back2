package com.homeplus.controllers;

import com.homeplus.dtos.comment.CommentGetDto;
import com.homeplus.dtos.comment.CommentPostDto;
import com.homeplus.dtos.comment.CommentPutDto;
import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.services.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
public class CommentController {
    private final CommentService commentService;

    @GetMapping("task-comments")
    public ResponseEntity<List<CommentGetDto>> find(@RequestParam Long id){
        return ResponseEntity.ok(commentService.getCommentsByTaskId(id));
    }

    @PostMapping("leave-comment")
    public ResponseEntity<String> createComment(@Valid @RequestBody CommentPostDto commentPostDto){
        commentService.createComment(commentPostDto);
        return ResponseEntity.ok("success");
    }

    @DeleteMapping("delete-comment")
    public ResponseEntity<String> deleteComment(@RequestParam Long id) {
        commentService.deleteComment(id);
        return ResponseEntity.ok("success");
    }

    @PutMapping("update-comment")
    public ResponseEntity<String> updateComment(@Valid @RequestBody CommentPutDto commentPutDto) {
        commentService.updateComment(commentPutDto);
        return ResponseEntity.ok("success");
    }
}
