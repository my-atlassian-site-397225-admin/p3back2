package com.homeplus.services;

import com.homeplus.dtos.follow.FollowGetDto;
import com.homeplus.dtos.follow.FollowPostDto;
import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskFollowEntity;
import com.homeplus.models.UserEntity;
import com.homeplus.repositories.TaskFollowRepository;
import com.homeplus.utility.mapper.TaskFollowMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskFollowService {

    private final TaskFollowRepository taskFollowRepository;
    private final TaskFollowMapper taskFollowMapper;
    private final TaskService taskService;
    private final UserService userService;

    public List<FollowGetDto> getFollowsByUserId(Long id) {
        UserEntity user = userService.getUserById(id);
        return taskFollowRepository.findByUser(user).stream()
                .map(follow -> taskFollowMapper.fromEntity(follow))
                .collect(Collectors.toList());
    }

    public void createFollow(FollowPostDto followPostDto) {
        followPostDto.setUserEntity(userService.findUserById(followPostDto.getUser_id()));
        followPostDto.setTaskEntity(taskService.getTaskEntityById(followPostDto.getTask_id()));

        Boolean isExists = isFollowExists(followPostDto.getUserEntity(), followPostDto.getTaskEntity());

        if (isExists) {
            TaskFollowEntity follow = getFollowByUserAndTask(followPostDto.getUserEntity(), followPostDto.getTaskEntity());
            follow.setFollowing(!follow.getFollowing());
            follow.setUpdated_time(OffsetDateTime.now());
            taskFollowRepository.save(follow);
        } else {
            taskFollowRepository.save(taskFollowMapper.postDtoToEntity(followPostDto));
        }
    }

    public TaskFollowEntity getFollowByUserAndTask(UserEntity user, TaskEntity task) {
        return taskFollowRepository.findByUserAndTask(user, task).get();
    }

    public boolean isFollowExists(UserEntity user, TaskEntity task) {
        return taskFollowRepository.findByUserAndTask(user, task).isPresent();
    }

}
