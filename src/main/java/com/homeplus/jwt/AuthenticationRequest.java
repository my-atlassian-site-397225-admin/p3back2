package com.homeplus.jwt;

import lombok.Data;

@Data
public class AuthenticationRequest {

    private String email;
    private String password;
}
