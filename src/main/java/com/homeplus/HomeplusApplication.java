package com.homeplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class HomeplusApplication {
	public static void main(String[] args) {
		SpringApplication.run(HomeplusApplication.class, args);
	}
}
