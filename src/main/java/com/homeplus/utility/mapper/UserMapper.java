package com.homeplus.utility.mapper;

import com.homeplus.dtos.AccountDto;
import com.homeplus.models.Status;
import com.homeplus.models.UserEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

import static java.time.ZoneOffset.UTC;

@Slf4j
@RequiredArgsConstructor
@Component
public class UserMapper {

    private final PasswordEncoder passwordEncoder;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserEntity mapInfoDtoToEntity(AccountDto accountDto) {
//        String encodedPassword = passwordEncoder.encode(accountDto.getPassword());
        String encodedPassword = bCryptPasswordEncoder.encode(accountDto.getPassword());
        log.info("Register encoded password: " + encodedPassword);
        return UserEntity.builder()
                .name(accountDto.getName())
                .email(accountDto.getEmail().toLowerCase())
                .status("UNVERIFIED")
                .is_tasker(false)
                .is_tasker_data(false)
                .createdTime(OffsetDateTime.now(UTC))
                .updatedTime(OffsetDateTime.now(UTC))
                .build();
    }

}
